import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:homey_kitchen/screens/navigation_screen/navigation_screen.dart';

import '../cubits/splash_cubit/splash_cubit.dart';
import '../screens/splash_screen/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  // ======Start App=====//
  '/': (context) => BlocProvider(
        create: (context) => SplashCubit()..checkConnection(),
        child: const SplashScreen(),
      ),
  // ======Start App=====//
  '/navigation-screen': (context) => const NavigationScreen()
};
