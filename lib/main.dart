import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:homey_kitchen/core/shared/local_network.dart';
import 'package:homey_kitchen/routes/routes.dart';
import 'package:homey_kitchen/theme/theme.dart';

import 'core/notification/notification.dart';

void main() async {
  await WidgetsFlutterBinding.ensureInitialized();
  await CashNetwork.cashInitialization();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: 'AIzaSyCpqL7hvVLTCa1Nh12eM0q_J2pCP1LHEpo',
      appId: '1:838693518963:android:8ddc624fd40948843eb406',
      messagingSenderId: '838693518963',
      projectId: 'flutter-1cda7',
    ),
  );
  final fcm = await FirebaseMessaging.instance.getToken();
  print('The fcm token is => $fcm');
  await FirebaseApi().initNotification();
  runApp(
    Phoenix(
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Homey Kitchen',
      initialRoute: '/',
      routes: routes,
      theme: themeData,
    );
  }
}
