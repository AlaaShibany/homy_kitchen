import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:homey_kitchen/constants/color.dart';

final ThemeData themeData = ThemeData(
  //========= General Theme =========//

  primaryColor: AppColors.wight,
  scaffoldBackgroundColor: Colors.white,
  visualDensity: VisualDensity.standard,
  useMaterial3: true,

  //======= Text Theme =======/
  textTheme: TextTheme(
    bodyLarge: GoogleFonts.tajawal(color: Colors.black),
    bodyMedium: GoogleFonts.tajawal(color: Colors.black),
    titleLarge: GoogleFonts.tajawal(color: Colors.black),
  ),

  //==========Button Theme ========///
  buttonTheme: const ButtonThemeData(
    textTheme: ButtonTextTheme.primary,
  ),
  // colorScheme:
  //     ColorScheme.fromSeed(seedColor: const Color.fromARGB(255, 6, 121, 141)),

  //======Icon Theme ========//
  iconTheme: const IconThemeData(
    color: AppColors.gray,
    size: 30,
  ),

  //========= List tile Theme =======//
  listTileTheme: const ListTileThemeData(
    // tileColor: AppColor.darkBlue,
    // selectedTileColor: AppColor.darkBlue,
    // contentPadding: EdgeInsets.all(16),
    iconColor: AppColors.gray,
  ),
  //========= text filed Theme =======//
  inputDecorationTheme: InputDecorationTheme(
    hintStyle: GoogleFonts.tajawal(color: Colors.grey.withOpacity(0.5)),
    labelStyle: GoogleFonts.tajawal(color: AppColors.gray),
    focusColor: AppColors.gray,
    hoverColor: AppColors.gray,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(8.0),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: const BorderSide(color: AppColors.gray),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: const BorderSide(color: AppColors.gray),
    ),
  ),
  //===== App Bar======//
  appBarTheme: AppBarTheme(
    titleTextStyle: GoogleFonts.tajawal(
      fontSize: 20,
      color: Colors.white,
    ),
    backgroundColor: AppColors.gray,
    iconTheme: const IconThemeData(
      color: Colors.white,
    ),
  ),
  //======= Tab Bar Theme =======//
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
        backgroundColor:
            AppColors.primaryColor, // This is the button background color
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        textStyle: GoogleFonts.tajawal(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        )),
  ),
  //======= Tab Bar Theme =======//
  tabBarTheme: TabBarTheme(
    labelColor: Colors.black,
    unselectedLabelColor: Colors.black,
    labelStyle: GoogleFonts.tajawal(
      color: Colors.black,
    ),
    indicator: const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(
        Radius.circular(5),
      ),
    ),
    indicatorSize: TabBarIndicatorSize.tab,
    dividerColor: Colors.white,
    labelPadding: const EdgeInsets.symmetric(horizontal: 16.0),
  ),
);
