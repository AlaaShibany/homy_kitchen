import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class BadgeShimmer extends StatelessWidget {
  const BadgeShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: List.generate(
          10,
          (index) {
            return Shimmer.fromColors(
              baseColor: Colors.grey[100]!,
              highlightColor: Colors.grey[300]!,
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: mediaQuery.width / 40,
                  vertical: mediaQuery.height / 150,
                ),
                height: mediaQuery.height / 5.4,
                width: mediaQuery.width,
                decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
