import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:quickalert/quickalert.dart';

import '../../../constants/color.dart';

void loadingDialog({required BuildContext context, required Size mediaQuery}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      content: SpinKitFadingCircle(
        color: AppColors.primaryColor,
        size: mediaQuery.width / 8,
      ),
    ),
  );
}

void internetToast({
  required BuildContext context,
}) {
  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
    content: Text('No internet connection. Please check your settings.'),
  ));
}

void serverToast({
  required BuildContext context,
}) {
  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
    content: Text("Server is down"),
  ));
}

void internetDialog({required BuildContext context, required Size mediaQuery}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
      backgroundColor: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15.0),
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            image: const AssetImage('assets/images/logo.png'),
            fit: BoxFit.contain,
            width: mediaQuery.width / 3,
            height: mediaQuery.height / 5,
          ),
          const Text(
            "Please make sure you are connected to the Internet",
            softWrap: true,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              color: AppColors.primaryColor,
            ),
          ),
        ],
      ),
    ),
  );
}

void errorDialog({
  required BuildContext context,
  required String text,
}) {
  QuickAlert.show(
    context: context,
    type: QuickAlertType.error,
    text: text,
    confirmBtnColor: Colors.red,
  );
}

void infoDialog({
  required BuildContext context,
  required String text,
}) {
  QuickAlert.show(
    context: context,
    type: QuickAlertType.info,
    text: text,
    confirmBtnColor: Colors.amber.shade400,
  );
}

void successDialog({
  required BuildContext context,
  required String text,
}) {
  QuickAlert.show(
    context: context,
    type: QuickAlertType.success,
    text: text,
    confirmBtnColor: Colors.green,
  );
}
