import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:homey_kitchen/core/shared/connect.dart';
import 'package:homey_kitchen/core/shared/local_network.dart';
import 'package:meta/meta.dart';

part 'splash_state.dart';

class SplashCubit extends Cubit<SplashState> {
  SplashCubit() : super(SplashInitial());
  int checkInternetCounter = -1;

  Future<void> checkConnection() async {
    await Future.delayed(const Duration(seconds: 2));
    print('Found internet');
    if (await checkInternet()) {
      checkLoginStatus();
    } else {
      checkInternetCounter++;
      emit(SplashNoInternetConnection());
    }
  }

  Future<void> checkLoginStatus() async {
    try {
      String? token = await CashNetwork.getCashData(key: 'token');
      print('stored token => $token');
      if (token.isEmpty) {
        print('check state is => Don\'t found a token');
        await Future.delayed(const Duration(seconds: 2));
        emit(SplashLoginState(isLogIn: false));
      } else {
        print('check state is => found a token : $token');
        await Future.delayed(const Duration(seconds: 2));
        emit(SplashLoginState(isLogIn: true));
      }
    } catch (e) {
      print(e);
    }
  }
}
