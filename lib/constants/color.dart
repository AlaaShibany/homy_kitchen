import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color.fromARGB(255, 255, 145, 0);
  static const secondColor = Color.fromARGB(255, 253, 179, 82);
  static const gray = Color(0xFF3B3B3B);
  static const darkRed = Color.fromARGB(255, 162, 23, 23);
  static const glow = Color.fromARGB(255, 216, 212, 206);
  static const wight = Color.fromARGB(255, 243, 243, 243);
  static const dark = Color.fromARGB(255, 37, 46, 50);
  static const darkBlue = Color.fromARGB(255, 25, 63, 80);
}
