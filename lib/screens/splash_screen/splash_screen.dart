import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:homey_kitchen/constants/color.dart';
import 'package:homey_kitchen/core/animation/dialogs/dialogs.dart';
import 'package:homey_kitchen/core/animation/transition.dart';
import 'package:homey_kitchen/cubits/splash_cubit/splash_cubit.dart';
import 'package:homey_kitchen/screens/navigation_screen/navigation_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    final cubit = context.read<SplashCubit>();
    return Scaffold(
      body: BlocConsumer<SplashCubit, SplashState>(
        listener: (context, state) {
          if (state is SplashLoginState) {
            if (cubit.checkInternetCounter >= 0) {
              Navigator.pop(context);
            }
            Navigator.of(context).pushAndRemoveUntil(
                customPageRouteBuilder(const NavigationScreen()),
                (Route<dynamic> route) => false);
            ;
          } else if (state is SplashNoInternetConnection) {
            if (cubit.checkInternetCounter == 0) {
              internetDialog(context: context, mediaQuery: mediaQuery);
            }
            cubit.checkConnection();
          }
        },
        builder: (context, state) {
          return Container(
            height: mediaQuery.height,
            width: mediaQuery.width,
            decoration: const BoxDecoration(
              gradient: LinearGradient(colors: [
                AppColors.primaryColor,
                AppColors.primaryColor,
                AppColors.secondColor,
                AppColors.primaryColor,
                AppColors.primaryColor,
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Image(image: AssetImage('assets/images/logo.png')),
                Text(
                  'Homey Kitchen',
                  style: TextStyle(
                    fontFamily: 'HarlowSolid',
                    fontSize: mediaQuery.width / 10,
                    color: Colors.white,
                    shadows: const [
                      BoxShadow(
                        color: Colors.black54,
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
