import 'package:flutter/material.dart';

class NavigationScreen extends StatelessWidget {
  const NavigationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'This is Maiandra GD font',
            style: TextStyle(fontFamily: 'MaiandraGD', fontSize: 24),
          ),
          Text(
            'This is Harlow Solid font',
            style: TextStyle(fontFamily: 'HarlowSolid', fontSize: 24),
          ),
        ],
      ),
    );
  }
}
